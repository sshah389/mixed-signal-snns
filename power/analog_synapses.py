import torch.nn as nn
import torch

dtype = torch.float
device=torch.device('cuda')

'''
Floating-Gate Inspired Linear Layer with Power Calculation
'''
class non_linear(nn.Module):
    def __init__(self, size_in, size_out, calc_power = False):
        super().__init__()
        self.size_in, self.size_out = size_in, size_out

        weight = torch.Tensor(size_out, size_in)
        self.weight = nn.Parameter(weight)
        
        weights_sub = torch.Tensor(size_out, size_in)
        self.weights_sub = nn.Parameter(weights_sub)
        
        bias = torch.Tensor(size_out)
        self.bias = nn.Parameter(bias)
        self.calc_power = calc_power
        
        nn.init.normal_(self.weights_sub, mean=0.35, std=0.1)
        nn.init.normal_(self.weight, mean=0.35, std=0.1)
        nn.init.normal_(self.bias, mean=0.35, std=0.1)  # bias init

        self.register_buffer('total_power', torch.Tensor([0]), persistent=False)
        self.n_inference = 0
        
    def forward(self, x):
            Fg_current_weights_pre=FG_current_weights_calc(self.weight)
            Fg_current_weights1_pre=FG_current_weights_calc(self.weights_sub)
            Fg_current_weights= torch.mm(x,Fg_current_weights_pre.t())
            Fg_current_weights1= torch.mm(x,Fg_current_weights1_pre.t())
            Fg_current_bias= FG_current_bias_calc(self.bias)
            Fg_current1= torch.add(Fg_current_weights,Fg_current_bias)
            Iout1= (Fg_current1)#to invert the output
            Iout2= (Fg_current_weights1)#to invert the output
            Iout3= torch.sub(Iout1,Iout2)

            '''
            Power Calculation
            '''
            if (self.calc_power):
                p1 = Iout3.mul(1)

                #this assumes the batch is the 0th dim (batch dim = 0)
                batch_inference_power = (p1.flatten(start_dim=1).sum(dim=1)).detach()                              

                self.total_power += batch_inference_power.mean()  
                self.n_inference += 1

            return Iout3
    
    def enable_power_calc(self):
         self.calc_power = True
         self.total_power.fill_(0.0)
         self.n_inference = 0

    def get_avg_power(self, num_images = 10000, num_time_steps = 100, units = 1000):
        return (self.total_power/(num_images*num_time_steps*units)).item()
    
    '''
    Area Calculation
    '''   
    def get_area_size(self):
        # Each Floating Gate is 45.38 micro-meters, this synapse contains two
        # Floating-Gates, so is multiplied by two.
        return (45.38 * 2)
    
'''
Floating-Gate Weights Calculation
'''
def FG_current_weights_calc(weights):
        kappa=0.61
        Ithpmos=270e-9
        vdd=3
        vs=3
        L=1e-6
        Ctun=9.360e-16
        Cov=1.2920e-17
        Cox=2.34e-15
        Cgso=119.9e-12*L
        Cgdo=119.9e-12*L
        sigma=0.01
        Vtp=.7
        Vtun=1.2
        Ut=25.9e-3
        Cin=1.7557e-14
        vd=0;

        CT=Cin+Cgdo+Cgso+Cov+Cox*(1-kappa)+Ctun
        vfg_term= ((Cgdo/CT)*vd) + ((Cgso/CT)*vs) + (Ctun/CT)*Vtun
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp-vfg_term
        vdd_vfg_vt=torch.add(-weights,vdd_vt)
        
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut).to(device)
        weight_exp=torch.exp(vfg).to(device)
        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)
        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)

        input_exp=torch.exp(torch.mul(0.2,kappa_mult_Cin_div_Ct_div_Ut)).to(device)
        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)

        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)
        Fg_current= torch.sub(current1,current2)

        Fg_current = Fg_current.type(dtype=dtype)
        Fg_current = torch.nan_to_num(Fg_current, nan=0.0, posinf=0.001, neginf=-0.001)

        return Fg_current
'''
Floating-Gate Bias Calculation
'''
def FG_current_bias_calc(bias):
        kappa=0.61
        Ithpmos=270e-9
        vdd=3
        vs=3
        L=1e-6
        Ctun=9.360e-16
        Cov=1.2920e-17
        Cox=2.34e-15
        Cgso=119.9e-12*L
        Cgdo=119.9e-12*L
        sigma=0.01
        Vtp=.7
        Vtun=1.2
        Ut=25.9e-3
        Cin=1.7557e-14
        vd=0;

        CT=Cin+Cgdo+Cgso+Cov+Cox*(1-kappa)+Ctun
        vfg_term= ((Cgdo/CT)*vd) + ((Cgso/CT)*vs) + (Ctun/CT)*Vtun
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp-vfg_term

        vdd_vfg_vt=torch.add(-bias,vdd_vt)
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut)
        weight_exp=torch.exp(vfg).to(device)
        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)

        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)
        input_exp=torch.exp(torch.mul(0.2,kappa_mult_Cin_div_Ct_div_Ut)).to(device)
        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)

        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)
        Fg_current= torch.sub(current1,current2)

        Fg_current = Fg_current.type(dtype=dtype)
        Fg_current = torch.nan_to_num(Fg_current, nan=0.0, posinf=0.001, neginf=-0.001)
        
        return Fg_current