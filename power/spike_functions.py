import torch.nn as nn
import torch

class FastSigmoid(torch.autograd.Function):
    @staticmethod
    def forward(ctx, input_):
        ctx.save_for_backward(input_)
        return  (input_>0).type(input_.dtype)

    @staticmethod
    def backward(ctx, grad_output):
        (input_,) = ctx.saved_tensors
        grad_input = grad_output.clone()
        return grad_input / (10 * torch.abs(input_) + 1.0) ** 2
    
class SmoothStep(torch.autograd.Function):

    @staticmethod
    def forward(aux, x):
        aux.save_for_backward(x)
        return (x >= 0).type(x.dtype)
    
    @staticmethod
    def backward(aux, grad_output):
        # grad_input = grad_output.clone()
        input, = aux.saved_tensors
        grad_input = grad_output.clone()
        grad_input[input <= -.5] = 0
        grad_input[input > .5] = 0
        return grad_input

#Callable PyTorch functions
smooth_step = SmoothStep().apply
sigmoid = nn.Sigmoid()
fast_sigmoid = FastSigmoid.apply