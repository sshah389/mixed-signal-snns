import torch.nn as nn
import torch

dtype = torch.float
device=torch.device('cuda')

'''
Floating-Gate Inspired Linear Layer
'''
class non_linear(nn.Module):
    def __init__(self, size_in, size_out):
        super().__init__()
        self.size_in, self.size_out = size_in, size_out

        weight = torch.Tensor(size_out, size_in)
        self.weight = nn.Parameter(weight)
        
        weights_sub = torch.Tensor(size_out, size_in)
        self.weights_sub = nn.Parameter(weights_sub)
        
        bias = torch.Tensor(size_out)
        self.bias = nn.Parameter(bias)
        
        nn.init.normal_(self.weights_sub, mean=0.35, std=0.1)
        nn.init.normal_(self.weight, mean=0.35, std=0.1)
        nn.init.normal_(self.bias, mean=0.35, std=0.1)  # bias init
        
    def forward(self, x):
            Fg_current_weights_pre=FG_current_weights_calc(self.weight)
            Fg_current_weights1_pre=FG_current_weights_calc(self.weights_sub)
            Fg_current_weights=torch.mm(x,Fg_current_weights_pre.t())
            Fg_current_weights1=torch.mm(x,Fg_current_weights1_pre.t())
            Fg_current_bias=FG_current_bias_calc(self.bias)
            Fg_current1=torch.add(Fg_current_weights,Fg_current_bias)
            Iout1=(Fg_current1)#to invert the output
            Iout2=(Fg_current_weights1)#to invert the output
            Iout3=torch.sub(Iout1,Iout2)
            return Iout3

'''
Floating-Gate Weights Calculation
'''
def FG_current_weights_calc(weights):
        kappa=0.61
        Ithpmos=270e-9
        vdd=3
        vs=3
        L=1e-6
        Ctun=9.360e-16
        Cov=1.2920e-17
        Cox=2.34e-15
        Cgso=119.9e-12*L
        Cgdo=119.9e-12*L
        sigma=0.01
        Vtp=.7
        Vtun=1.2
        Ut=25.9e-3
        Cin=1.7557e-14
        vd=0;
        
        CT=Cin+Cgdo+Cgso+Cov+Cox*(1-kappa)+Ctun
        vfg_term= ((Cgdo/CT)*vd) + ((Cgso/CT)*vs) + (Ctun/CT)*Vtun
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp-vfg_term
        
        vdd_vfg_vt=torch.add(-weights,vdd_vt)
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut).to(device)
        weight_exp=torch.exp(vfg).to(device)
        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)

        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)
        input_exp=torch.exp(torch.mul(0.2,kappa_mult_Cin_div_Ct_div_Ut)).to(device)
        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)

        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)
        Fg_current= torch.sub(current1,current2)

        Fg_current = Fg_current.type(dtype=dtype)
        return Fg_current
 
'''
Floating-Gate Bias Calculation
'''
def FG_current_bias_calc(bias):
        
        kappa=0.61
        Ithpmos=270e-9
        vdd=3
        vs=3
        L=1e-6
        Ctun=9.360e-16
        Cov=1.2920e-17
        Cox=2.34e-15
        Cgso=119.9e-12*L
        Cgdo=119.9e-12*L
        sigma=0.01
        Vtp=.7
        Vtun=1.2
        Ut=25.9e-3
        Cin=1.7557e-14
        vd=0;

        CT=Cin+Cgdo+Cgso+Cov+Cox*(1-kappa)+Ctun
        vfg_term= ((Cgdo/CT)*vd) + ((Cgso/CT)*vs) + (Ctun/CT)*Vtun
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp-vfg_term
        vdd_vfg_vt=torch.add(-bias,vdd_vt)
        
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut)
        weight_exp=torch.exp(vfg).to(device)
        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)

        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)
        input_exp=torch.exp(torch.mul(0.2,kappa_mult_Cin_div_Ct_div_Ut)).to(device)
        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)

        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)
        Fg_current= torch.sub(current1,current2)

        Fg_current = Fg_current.type(dtype=dtype)
        return Fg_current

'''
Re-RAM Inspired Linear Layer
'''
class Re_RAM_Fully_Connected(nn.Module):
    def __init__(self, size_in, size_out, gate_voltage=0.7):
        super().__init__()
        self.size_in, self.size_out = size_in, size_out
        self.gate_voltage = gate_voltage

        weight = torch.Tensor(size_out, size_in)
        self.weight = nn.Parameter(weight)
        
        bias = torch.Tensor(size_out)
        self.bias = nn.Parameter(bias)
        
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)
        
    def forward(self, input):
      MAX_V = 1.0
      MAX_X = 1.0
      
      # From reram VerilogA model obtaind from Skywater (sky130)
      TFIL_REF = 4.7249e-9 # filament thickness calibration parameter
      I_k1     = 6.140e-5  # current calibration parameter in Amps
      Tox      = 5.0e-9    # thickness of oxide in meters
      V_ref    = 0.430     # Voltage calibration paramter in Volts

      w_fil_p, w_fil_n = Re_RAM_Current_Calc(self.weight)
      pos_bias, neg_bias = Re_RAM_Current_Calc(self.bias)

      input = MAX_V * input/MAX_X

      sinhx = torch.sinh(torch.Tensor([self.gate_voltage]) / V_ref).to(device)
      
      # The current through the ReRAM is caculated using the folowing 
      # formula from the Skywater ReRAM model (also found in various 
      # publications ).

      # I = I_k1 * exp(-(Tox - fil)/(Tox - TFIL_REF)) * sinh(V/V_ref)

      pos_current = nn.functional.linear(input= input, weight= (sinhx * I_k1 * torch.exp(-(Tox - w_fil_p)/(Tox - TFIL_REF))), bias= pos_bias)
      neg_current = nn.functional.linear(input= input, weight= (sinhx * I_k1 * torch.exp(-(Tox - w_fil_n)/(Tox - TFIL_REF))), bias= neg_bias)

      return (pos_current - neg_current)
    
    
'''
Re-RAM Current Calculation
'''
def Re_RAM_Current_Calc(input):
    MAX_W = 1.0
    MIN_W = -1.0

    # From reram VerilogA model obtaind from Skywater (sky130)
    TFIL_MAX = 4.9e-9    # max filament in meters
    TFIL_MIN = 3.3e-9    # min filament in meters

    ALPHA = TFIL_MAX - TFIL_MIN

    # Get the filament length for the current weight values
    # (split pos. and neg.).
    w_fil_p = ((input.clamp(0.0, MAX_W))/MAX_W) * ALPHA + TFIL_MIN
    w_fil_n = ((input.clamp(MIN_W, 0.0))/MIN_W) * ALPHA + TFIL_MIN

    return (w_fil_p, w_fil_n)