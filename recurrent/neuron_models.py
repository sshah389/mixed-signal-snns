from spike_functions import *
from analog_synapses import *

import torch.nn as nn
import torch
from collections import namedtuple
import warnings
from utils import get_output_shape

dtype = torch.float
device = torch.device('cuda')

'''
Leaky-Integrate and Fire Neuron Model
'''
class LIFLayer(nn.Module):
    NeuronState = namedtuple('NeuronState', ['P', 'Q', 'R', 'M' , 'S'])

    def __init__(self, layer, alpha=.9, alpharp=.65, wrp=1.0, beta=.85,gamma=.8, deltat=1000):
        super(LIFLayer, self).__init__()
        self.base_layer = layer
        self.alpha = torch.tensor(alpha)
        self.beta = torch.tensor(beta)
        self.gamma = torch.tensor(gamma)
        self.tau_m = torch.nn.Parameter(1. / (1 - self.alpha), requires_grad=False)
        self.tau_s = torch.nn.Parameter(1. / (1 - self.beta), requires_grad=False)
        self.tau_k = torch.nn.Parameter(1. / (1 - self.gamma), requires_grad=False)
        self.deltat = deltat
        self.alpharp = alpharp
        self.wrp = wrp
        self.state = None

    def cuda(self, device=None):
        '''
        Handle the transfer of the neuron state to cuda
        '''
        self = super().cuda(device)
        self.state = None
        self.base_layer = self.base_layer.cuda()
        return self

    def cpu(self, device=None):
        '''
        Handle the transfer of the neuron state to cpu
        '''
        self = super().cpu(device)
        self.state = None
        self.base_layer = self.base_layer.cpu()
        return self

    @staticmethod
    def reset_parameters(layer):
        
        if hasattr(layer, 'out_features'): 
            layer.weight.data[:]*=0
            if layer.bias is not None:
                layer.bias.data.uniform_(-1e-3,1e-3)
        elif hasattr(layer, 'size_out'): 
            #layer.weight.data.normal_(mean=0.2, std=0.1)
            layer.weight.data.uniform_(-0.2,0.2)
            #layer.weights_sub.data.normal_(mean=0.2, std=0.1)
            layer.weights_sub.data[:]*=0
            layer.bias.data.uniform_(-1e-3,1e-3) 
        else:
            warning.warn('Unhandled data type, not resetting parameters')
    
    @staticmethod
    def get_out_channels(layer):
        '''
        Wrapper for returning number of output channels in a LIFLayer
        '''
        if hasattr(layer, 'out_features'):
            return layer.out_features
        elif hasattr(layer, 'size_out'):
            return layer.size_out
        elif hasattr(layer, 'out_channels'): 
            return layer.out_channels
        elif hasattr(layer, 'get_out_channels'): 
            return layer.get_out_channels()
        else: 
            raise Exception('Unhandled base layer type')
    
    @staticmethod
    def get_out_shape(layer, input_shape):
        if hasattr(layer, 'out_channels'):
            return get_output_shape(input_shape, 
                                    kernel_size=layer.kernel_size,
                                    stride = layer.stride,
                                    padding = layer.padding,
                                    dilation = layer.dilation)
        
        elif hasattr(layer, 'out_features'):
            return []
        elif hasattr(layer, 'size_out'): 
            return []
        elif hasattr(layer, 'get_out_shape'): 
            return layer.get_out_shape()
        else: 
            raise Exception('Unhandled base layer type')

    def init_state(self, Sin_t):
        device = self.base_layer.weight.device
        input_shape = list(Sin_t.shape)
        out_ch = self.get_out_channels(self.base_layer)
        out_shape = self.get_out_shape(self.base_layer, input_shape)
        self.state = self.NeuronState(P=torch.zeros(input_shape).type(dtype).to(device),
                                      Q=torch.zeros(input_shape).type(dtype).to(device),
                                      R=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      M=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      S=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device))

    def init_parameters(self):
        self.reset_parameters(self.base_layer)


    def forward(self, Sin_t):
        if self.state is None:
            self.init_state(Sin_t)

        state = self.state
        Q = self.beta * state.Q + self.tau_s * Sin_t * 100
        M= self.gamma * state.M + self.tau_k * state.S # * 0.1 * 7e-12 + 0.5 * state.P
        P = self.alpha * state.P + self.tau_m * state.Q # self.tau_m * state.M # TODO check with Emre: Q or state.Q?
        R = self.alpharp * state.R - state.S * self.wrp
        U = self.base_layer(P) + R - M
        S = fast_sigmoid(U)
        self.state = self.NeuronState(P=P.detach(), Q=Q.detach(), R=R.detach(), M=M.detach(), S=S.detach())
        return S, U

    def get_output_shape(self, input_shape):
        layer = self.base_layer
        if not isinstance(layer, nn.Conv2d):
            raise TypeError('You can only get the output shape of Conv2d layers. Please change layer type')
        im_height = input_shape[-2]
        im_width = input_shape[-1]
        height = int((im_height + 2 * layer.padding[0] - layer.dilation[0] *
                      (layer.kernel_size[0] - 1) - 1) // layer.stride[0] + 1)
        weight = int((im_width + 2 * layer.padding[1] - layer.dilation[1] *
                      (layer.kernel_size[1] - 1) - 1) // layer.stride[1] + 1)
        return [height, weight]
    
    def get_device(self):
        return self.base_layer.weight.device

'''
Recurrent Leaky Integrate and Fire Neuron
'''
class RecLIFLayer(LIFLayer):
    NeuronState = namedtuple('NeuronState', ['P', 'Pr', 'Q', 'Qr', 'R','S'])
    def __init__(self, *args, **kwargs):
        super(RecLIFLayer, self).__init__(*args, **kwargs)
        self.rec_layer = non_linear(self.base_layer.size_out, self.base_layer.size_out)
    
    def cuda(self, device=None):
        '''
        Handle the transfer of the neuron state to cuda
        '''
        self = super().cuda(device)
        self.rec_layer = self.rec_layer.to(device)
        return self

    def cpu(self, device=None):
        '''
        Handle the transfer of the neuron state to cpu
        '''
        self = super().cpu(device)
        self.rec_layer = self.rec_layer.cpu()
        return self
    
    def init_state(self, Sin_t):
        device = self.base_layer.weight.device
        input_shape = list(Sin_t.shape)
        out_ch = self.get_out_channels(self.base_layer)
        out_shape = self.get_out_shape(self.base_layer, input_shape)
        self.state = self.NeuronState(P=torch.zeros(input_shape).type(dtype).to(device),
                                      Q=torch.zeros(input_shape).type(dtype).to(device),
                                      Pr=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      Qr=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      R =torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      S=torch.zeros([out_ch] + out_shape).type(dtype).to(device))
        
    def forward(self, Sin_t, Inj=0):
        if self.state is None:
            self.init_state(Sin_t)

        #Forward traces
        state = self.state
        P = self.alpha * state.P + (1-self.alpha)*state.Q
        Q = self.beta  * state.Q + (1-self.beta) *Sin_t*100

        #Recurrent traces
        Pr = self.alpha * state.Pr + (1-self.alpha)*state.Qr
        Qr = self.beta  * state.Qr + (1-self.beta) *state.S

        #Refractory
        R = self.alpharp * state.R - state.S * self.wrp

        #Membrane potential
        U = self.base_layer(P) + self.rec_layer(Pr) + R 
        S = fast_sigmoid(U)
        self.state = self.NeuronState(P=P.detach(),
                                      Q=Q.detach(),
                                      Pr=Pr.detach(),
                                      Qr=Qr.detach(),
                                      R= R.detach(),
                                      S= S.detach())
            
        return S, U
    def init_parameters(self, *args, **kwargs):
        self.reset_parameters(self.base_layer, *args, **kwargs)
        self.reset_rec_parameters(self.rec_layer, *args, **kwargs)

    def reset_parameters(self, layer):
        #layer.reset_parameters()
        if hasattr(layer, 'out_channels'):
            if layer.bias is not None:
                layer.bias.data = layer.bias.data*((1-self.alpha)*(1-self.beta))
            layer.weight.data[:] *= 1
        elif hasattr(layer, 'size_out'): 
            if layer.bias is not None:
                layer.bias.data.uniform_(-1e-3,1e-3)
            layer.weight.data.uniform_(-0.2,0.2)
            layer.weights_sub.data[:]*=0
        else:
            warnings.warn('Unhandled data type, not resetting parameters')

    def reset_rec_parameters(self, layer):
        #layer.reset_parameters()
        if hasattr(layer, 'out_channels'):
            if layer.bias is not None:
                layer.bias.data.uniform_(-1e-3,1e-3)
            layer.weight.data *= 0 
        elif hasattr(layer, 'size_out'): 
            if layer.bias is not None:
                layer.bias.data.uniform_(-1e-3,1e-3)
            layer.weights_sub.data[:]*=0
            layer.weight.data.uniform_(-0.2,0.2)
        else:
            warnings.warn('Unhandled data type, not resetting parameters')

'''
Neuron Utils
'''
def tonp(tensor): 
    if type(tensor) == type([]): 
        return [t.detach().cpu().numpy() for t in tensor] 
    elif not hasattr(tensor, 'detach'): 
        return tensor 
    else: 
        return tensor.detach().cpu().numpy()