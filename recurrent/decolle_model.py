import torch.nn as nn
import numpy as np
from itertools import chain

'''
DECOLLE Network Module
'''
class DECOLLEBase(nn.Module):
    requires_init = True
    def __init__(self):
        super(DECOLLEBase, self).__init__()
        self.LIF_layers = nn.ModuleList()
        self.readout_layers = nn.ModuleList()
        self.non_linear=nn.ModuleList()
        
    def __len__(self):
        return len(self.LIF_layers)

    def forward(self, input):
        raise NotImplemented('')
    
    @property
    def output_layer(self):
        return self.readout_layers[-1]

    def get_trainable_parameters(self, layer=None):
        if layer is None:
            return chain(*[l.parameters() for l in self.LIF_layers])
        else:
            return self.LIF_layers[layer].parameters()

    def init(self, data_batch, burnin):
        '''
        Necessary to reset the state of the network whenever a new batch is presented
        '''
        if self.requires_init is False:
            return
        for l in self.LIF_layers:
            l.state = None
        for i in range(max(len(self), burnin)):
            self.forward(data_batch[:, i, :, :])

    def init_parameters(self, data_batch):
        Sin_t = data_batch[:, 0, :, :]
        s_out,_ = self.forward(Sin_t)[:2]
        for i,l in enumerate(self.LIF_layers):
            l.init_parameters()

    def reset_lc_parameters(self, layer, lc_ampl):
        stdv = lc_ampl / np.sqrt(layer.weight.size(1))
        layer.weight.data.uniform_(-stdv, stdv)
        if layer.bias is not None:
            layer.bias.data.uniform_(-stdv, stdv)
    
    def get_input_layer_device(self):
        if hasattr(self.LIF_layers[0], 'get_device'):
            return self.LIF_layers[0].get_device() 
        else:
            return list(self.LIF_layers[0].parameters())[0].device

    def get_output_layer_device(self):
        return self.output_layer.weight.device