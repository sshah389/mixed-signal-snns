import torch.nn as nn
import torch
import math

dtype = torch.float
device = torch.device('cuda')

'''
Floating-Gate Inspired Convolution Layer
'''
class conv_fg(nn.Module):
    def __init__(self, in_channels=100, out_channels=16, size=3, stride=1, padding=0):
        super().__init__()
        self.size = size
        self.padding=padding
        self.stride = stride
        self.out_channels=out_channels
        self.in_channels=in_channels
        conv1 = nn.Conv2d(in_channels, out_channels, size, stride,padding, bias=False)
        conv2 = nn.Conv2d(in_channels, out_channels, size, stride,padding, bias=False)
        self.weight = conv1.weight
        self.weight2 = conv2.weight

    def forward(self, input):
        m = nn.ZeroPad2d(self.padding)
        img=m(input)
        batch_size, channels, h, w = img.shape
        # setup the parameters for Conv2d

        kh, kw = self.size, self.size # kernel size
        dh, dw = self.stride, self.stride # stride
        patches = img.unfold(2, kh, dh).unfold(3, kw, dw)

        # batch_size, channels, h_windows, w_windows, kh, kw
        patches = patches.contiguous().view(batch_size, channels, -1, kh, kw)

        # Shift the windows into the batch dimension using permute
        patches = patches.permute(0, 2, 1, 3, 4)
        # batch_size, nb_windows, channels, kh, kw

        # Multiply the patches with the weights in order to calculate the conv
        result1= (FG_conv_current_weights_calc(patches.unsqueeze(2), self.weight.unsqueeze(0).unsqueeze(1))).sum([3, 4, 5])
        result2= (FG_conv_current_weights_calc(patches.unsqueeze(2), self.weight2.unsqueeze(0).unsqueeze(1))).sum([3, 4, 5])
        # batch_size, output_pixels, out_channels

        result = torch.sub(result1,result2)
        result = result.permute(0, 2, 1)
        # batch_size, out_channels, output_pixels

        # assuming h = w
        h = w = int(result.size(2)**0.5)
        result = result.view(batch_size, -1, h, w)

        return result 

'''
Floating-Gate Convolution Weights Calculation
'''
def FG_conv_current_weights_calc(input, weights):
        kappa=0.4145
        Ithpmos=3.36e-7
        vdd=3
        vs=2.7
        Cw=1.2920e-17
        Cov=1.2920e-17
        Cox=1.7496e-15
        sigma=0.005
        Vtp=.8
        Ut=25.3e-3
        Cin=1.7557e-14

        CT=Cin+Cw+Cov+Cox*(1-kappa)
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp
        vdd_vfg_vt=torch.add(-weights,vdd_vt)
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut)
        weight_exp=torch.exp(vfg).to(device)

        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)
        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)
        input_exp=torch.exp(torch.mul(input,kappa_mult_Cin_div_Ct_div_Ut)).to(device)

        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)
        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)

        Fg_current= torch.sub(current1,current2).to(device)

        return Fg_current


'''
Floating-Gate Inspired Linear Layer
'''
class non_linear(nn.Module):
    def __init__(self, size_in, size_out):
        super().__init__()
        self.size_in, self.size_out = size_in, size_out

        weight = torch.Tensor(size_out, size_in)
        self.weight = nn.Parameter(weight)
        
        weights_sub = torch.Tensor(size_out, size_in)
        self.weights_sub = nn.Parameter(weights_sub)
        
        bias = torch.Tensor(size_out)
        self.bias = nn.Parameter(bias)
        
        nn.init.normal_(self.weights_sub, mean=0.35, std=0.1)
        nn.init.normal_(self.weight, mean=0.35, std=0.1)
        nn.init.normal_(self.bias, mean=0.35, std=0.1)  # bias init
        
    def forward(self, x):
            Fg_current_weights_pre=FG_current_weights_calc(self.weight)
            Fg_current_weights1_pre=FG_current_weights_calc(self.weights_sub)
            Fg_current_weights= torch.mm(x,Fg_current_weights_pre.t())
            Fg_current_weights1= torch.mm(x,Fg_current_weights1_pre.t())
            Fg_current_bias= FG_current_bias_calc(self.bias)
            Fg_current1= torch.add(Fg_current_weights,Fg_current_bias)
            Iout1= (Fg_current1)#to invert the output
            Iout2= (Fg_current_weights1)#to invert the output
            Iout3= torch.sub(Iout1,Iout2)
            return Iout3
            
'''
Floating-Gate Weights Calculation
'''
def FG_current_weights_calc(weights):
        kappa=0.61
        Ithpmos=270e-9
        vdd=3
        vs=3
        L=1e-6
        Ctun=9.360e-16
        Cov=1.2920e-17
        Cox=2.34e-15
        Cgso=119.9e-12*L
        Cgdo=119.9e-12*L
        sigma=0.01
        Vtp=.7
        Vtun=1.2
        Ut=25.9e-3
        Cin=1.7557e-14
        vd=0;

        CT=Cin+Cgdo+Cgso+Cov+Cox*(1-kappa)+Ctun
        vfg_term= ((Cgdo/CT)*vd) + ((Cgso/CT)*vs) + (Ctun/CT)*Vtun
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp-vfg_term
        vdd_vfg_vt=torch.add(-weights,vdd_vt)
        
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut).to(device)
        weight_exp=torch.exp(vfg).to(device)
        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)
        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)

        input_exp=torch.exp(torch.mul(0.2,kappa_mult_Cin_div_Ct_div_Ut)).to(device)
        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)

        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)
        Fg_current= torch.sub(current1,current2)

        Fg_current = Fg_current.type(dtype=dtype)
        return Fg_current

'''
Floating-Gate Bias Calculation
'''
def FG_current_bias_calc(bias):
        kappa=0.61
        Ithpmos=270e-9
        vdd=3
        vs=3
        L=1e-6
        Ctun=9.360e-16
        Cov=1.2920e-17
        Cox=2.34e-15
        Cgso=119.9e-12*L
        Cgdo=119.9e-12*L
        sigma=0.01
        Vtp=.7
        Vtun=1.2
        Ut=25.9e-3
        Cin=1.7557e-14
        vd=0;

        CT=Cin+Cgdo+Cgso+Cov+Cox*(1-kappa)+Ctun
        vfg_term= ((Cgdo/CT)*vd) + ((Cgso/CT)*vs) + (Ctun/CT)*Vtun
        kappa_div_Ut=kappa/(2*Ut)
        vdd_vt=vdd-Vtp-vfg_term

        vdd_vfg_vt=torch.add(-bias,vdd_vt)
        vfg=torch.mul(vdd_vfg_vt,kappa_div_Ut)
        weight_exp=torch.exp(vfg).to(device)
        s_vdd=torch.exp((sigma*torch.tensor([vdd]))/(2*Ut)).to(device)
        e_vdd=torch.exp((torch.tensor([-vdd])/(2*Ut))).to(device)

        kappa_mult_Cin_div_Ct_div_Ut=-(kappa*Cin)/(CT*2*Ut)
        input_exp=torch.exp(torch.mul(0.2,kappa_mult_Cin_div_Ct_div_Ut)).to(device)
        exp_part1=torch.mul(torch.mul(input_exp,weight_exp),s_vdd).to(device)
        exp_part2=torch.mul(torch.mul(input_exp,weight_exp),e_vdd).to(device)

        log_part1=torch.square(torch.log(torch.add(1,exp_part1)))
        log_part2=torch.square(torch.log(torch.add(1,exp_part2)))

        current1=torch.mul(log_part1,Ithpmos)
        current2=torch.mul(log_part2,Ithpmos)
        Fg_current= torch.sub(current1,current2)

        Fg_current = Fg_current.type(dtype=dtype)
        return Fg_current



'''
Re-RAM Inspired Convolution Layer
'''
class Re_RAM_Convolution(nn.Module):
    def __init__(self, in_channels=100, out_channels=16, size=3, stride=1, padding=0, gate_voltage=0.7):
        super().__init__()
        self.kernel_size = size
        self.padding = padding
        self.stride = stride
        self.dilation = 1
        self.out_channels = out_channels
        self.in_channels = in_channels
        conv = nn.Conv2d(in_channels, out_channels, size, stride, padding, bias= True)
        self.weight = conv.weight
        self.bias = conv.bias

        self.gate_voltage = gate_voltage
        
    def forward(self, input):
      MAX_V = 1.0
      MAX_X = 1.0
      
      # From reram VerilogA model obtaind from Skywater (sky130)
      TFIL_REF = 4.7249e-9 # filament thickness calibration parameter
      I_k1     = 6.140e-5  # current calibration parameter in Amps
      Tox      = 5.0e-9    # thickness of oxide in meters
      V_ref    = 0.430     # Voltage calibration paramter in Volts

      w_fil_p, w_fil_n = Re_RAM_Current_Calc(self.weight)
      pos_bias, neg_bias = Re_RAM_Current_Calc(self.bias)

      input = MAX_V * input/MAX_X
      sinhx = torch.sinh(torch.Tensor([self.gate_voltage]) / V_ref).to(device)

      # The current through the ReRAM is caculated using the folowing 
      # formula from the Skywater ReRAM model (also found in various 
      # publications ).

      # I = I_k1 * exp(-(Tox - fil)/(Tox - TFIL_REF)) * sinh(V/V_ref)

      pos_current = nn.functional.conv2d(input= input, weight= (sinhx * I_k1 * torch.exp(-(Tox - w_fil_p)/(Tox - TFIL_REF))), bias= pos_bias, 
                                         stride= self.stride, padding= self.padding)
      neg_current = nn.functional.conv2d(input= input, weight= (sinhx * I_k1 * torch.exp(-(Tox - w_fil_n)/(Tox - TFIL_REF))), bias= neg_bias,
                                         stride= self.stride, padding= self.padding)

      return (pos_current - neg_current)
    

'''
Re-RAM Inspired Linear Layer
'''
class Re_RAM_Fully_Connected(nn.Module):
    def __init__(self, size_in, size_out, gate_voltage=0.7):
        super().__init__()
        self.size_in, self.size_out = size_in, size_out
        self.gate_voltage = gate_voltage

        weight = torch.Tensor(size_out, size_in)
        self.weight = nn.Parameter(weight)
        
        bias = torch.Tensor(size_out)
        self.bias = nn.Parameter(bias)
        
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)
        
    def forward(self, input):
      MAX_V = 1.0
      MAX_X = 1.0
      
      # From reram VerilogA model obtaind from Skywater (sky130)
      TFIL_REF = 4.7249e-9 # filament thickness calibration parameter
      I_k1     = 6.140e-5  # current calibration parameter in Amps
      Tox      = 5.0e-9    # thickness of oxide in meters
      V_ref    = 0.430     # Voltage calibration paramter in Volts

      w_fil_p, w_fil_n = Re_RAM_Current_Calc(self.weight)
      pos_bias, neg_bias = Re_RAM_Current_Calc(self.bias)

      input = MAX_V * input/MAX_X

      sinhx = torch.sinh(torch.Tensor([self.gate_voltage]) / V_ref).to(device)
      
      # The current through the ReRAM is caculated using the folowing 
      # formula from the Skywater ReRAM model (also found in various 
      # publications ).

      # I = I_k1 * exp(-(Tox - fil)/(Tox - TFIL_REF)) * sinh(V/V_ref)

      pos_current = nn.functional.linear(input= input, weight= (sinhx * I_k1 * torch.exp(-(Tox - w_fil_p)/(Tox - TFIL_REF))), bias= pos_bias)
      neg_current = nn.functional.linear(input= input, weight= (sinhx * I_k1 * torch.exp(-(Tox - w_fil_n)/(Tox - TFIL_REF))), bias= neg_bias)

      return (pos_current - neg_current)
    
    
'''
Re-RAM Current Calculation
'''
def Re_RAM_Current_Calc(input):
    MAX_W = 1.0
    MIN_W = -1.0

    # From reram VerilogA model obtaind from Skywater (sky130)
    TFIL_MAX = 4.9e-9    # max filament in meters
    TFIL_MIN = 3.3e-9    # min filament in meters

    ALPHA = TFIL_MAX - TFIL_MIN

    # Get the filament length for the current weight values
    # (split pos. and neg.).
    w_fil_p = ((input.clamp(0.0, MAX_W))/MAX_W) * ALPHA + TFIL_MIN
    w_fil_n = ((input.clamp(MIN_W, 0.0))/MIN_W) * ALPHA + TFIL_MIN

    return (w_fil_p, w_fil_n)