import torch.nn as nn
import torch
import numpy as np
from collections import namedtuple
import warnings
from utils import get_output_shape
from spike_functions import *


dtype = torch.float
device=torch.device('cuda')

'''
Leaky-Integrate and Fire Neuron Model
'''
class LIFLayer(nn.Module):
    NeuronState = namedtuple('NeuronState', ['P', 'Q', 'R', 'M' , 'S'])

    def __init__(self, layer, alpha=.9, alpharp=.65, wrp=1.0, beta=.85,gamma=.8, deltat=1000, gain= 1.0):
        super(LIFLayer, self).__init__()
        self.base_layer = layer
        self.alpha = torch.tensor(alpha)
        self.beta = torch.tensor(beta)
        self.gamma = torch.tensor(gamma)
        self.tau_m = torch.nn.Parameter(1. / (1 - self.alpha), requires_grad=False)
        self.tau_s = torch.nn.Parameter(1. / (1 - self.beta), requires_grad=False)
        self.tau_k = torch.nn.Parameter(1. / (1 - self.gamma), requires_grad=False)
        self.deltat = deltat
        self.alpharp = alpharp
        self.wrp = wrp
        self.state = None
        self.gain = gain

    def cuda(self, device=None):
        '''
        Handle the transfer of the neuron state to cuda
        '''
        self = super().cuda(device)
        self.state = None
        self.base_layer = self.base_layer.cuda()
        return self

    def cpu(self, device=None):
        '''
        Handle the transfer of the neuron state to cpu
        '''
        self = super().cpu(device)
        self.state = None
        self.base_layer = self.base_layer.cpu()
        return self

    @staticmethod
    def reset_parameters(layer):
        
        if hasattr(layer, 'out_features'): 
            layer.weight.data[:]*=0
            if layer.bias is not None:
                layer.bias.data.uniform_(-1e-3,1e-3)
        elif hasattr(layer, 'out_channels'): 
            layer.weight.data[:]*=0
        elif hasattr(layer, 'size_out'): 
            #layer.weight.data.normal_(mean=0.2, std=0.1)
            layer.weight.data.uniform_(-0.2,0.2)
            #layer.weights_sub.data.normal_(mean=0.2, std=0.1)
            if hasattr(layer, 'weights_sub'):
                layer.weights_sub.data[:]*=0
            layer.bias.data.uniform_(-1e-3,1e-3) 
        else:
            warnings.warn('Unhandled data type, not resetting parameters')
    
    @staticmethod
    def get_out_channels(layer):
        '''
        Wrapper for returning number of output channels in a LIFLayer
        '''
        if hasattr(layer, 'out_features'):
            return layer.out_features
        elif hasattr(layer, 'size_out'):
            return layer.size_out
        elif hasattr(layer, 'out_channels'): 
            return layer.out_channels
        elif hasattr(layer, 'get_out_channels'): 
            return layer.get_out_channels()
        else: 
            raise Exception('Unhandled base layer type')
    
    @staticmethod
    def get_out_shape(layer, input_shape):
        if hasattr(layer, 'out_channels'):
            return get_output_shape(input_shape, 
                                    kernel_size=layer.kernel_size,
                                    stride = layer.stride,
                                    padding = layer.padding,
                                    dilation = layer.dilation)
        
        elif hasattr(layer, 'out_features'):
            return []
        elif hasattr(layer, 'size_out'): 
            return []
        elif hasattr(layer, 'get_out_shape'): 
            return layer.get_out_shape()
        else: 
            raise Exception('Unhandled base layer type')

    def init_state(self, Sin_t):
        device = self.base_layer.weight.device
        input_shape = list(Sin_t.shape)
        out_ch = self.get_out_channels(self.base_layer)
        out_shape = self.get_out_shape(self.base_layer, input_shape)
        self.state = self.NeuronState(P=torch.zeros(input_shape).type(dtype).to(device),
                                      Q=torch.zeros(input_shape).type(dtype).to(device),
                                      R=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      M=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device),
                                      S=torch.zeros([input_shape[0], out_ch] + out_shape).type(dtype).to(device))

    def init_parameters(self):
        self.reset_parameters(self.base_layer)


    def forward(self, Sin_t):
        if self.state is None:
            self.init_state(Sin_t)

        state = self.state

        Q = self.beta * state.Q + self.tau_s * (Sin_t * self.gain)
        M= self.gamma * state.M + self.tau_k * state.S # * 0.1 * 7e-12 + 0.5 * state.P
        P = self.alpha * state.P + self.tau_m * state.Q # self.tau_m * state.M # TODO check with Emre: Q or state.Q?
        R = self.alpharp * state.R - state.S * self.wrp

        U = self.base_layer(P) + R - M
        S = smooth_step(U)
        self.state = self.NeuronState(P=P.detach(), Q=Q.detach(), R=R.detach(), M=M.detach(), S=S.detach())


        return S, U

    def get_output_shape(self, input_shape):
        layer = self.base_layer
        if not isinstance(layer, nn.Conv2d):
            raise TypeError('You can only get the output shape of Conv2d layers. Please change layer type')
        im_height = input_shape[-2]
        im_width = input_shape[-1]
        height = int((im_height + 2 * layer.padding[0] - layer.dilation[0] *
                      (layer.kernel_size[0] - 1) - 1) // layer.stride[0] + 1)
        weight = int((im_width + 2 * layer.padding[1] - layer.dilation[1] *
                      (layer.kernel_size[1] - 1) - 1) // layer.stride[1] + 1)
        return [height, weight]
    
    def get_device(self):
        return self.base_layer.weight.device