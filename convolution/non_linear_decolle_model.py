from decolle_model import DECOLLEBase
from neuron_models import *
from spike_functions import *
from analog_synapses import *

import torch.nn as nn
import numpy as np

class LenetDECOLLE(DECOLLEBase):
    def __init__(self,
                 input_shape,
                 conv_channels=[1],
                 mlp_neurons=[128],
                 out_channels=1,
                 kernel_size=[7],
                 stride=[1],
                 pool_size=[2],
                 alpha=[.9],
                 beta=[.85],
                 gamma=[0.8],
                 alpharp=[.65],
                 dropout=[0.5],
                 num_conv_layers=2,
                 num_mlp_layers=1,
                 deltat=1000,
                 lc_ampl=.5,
                 gain= 1.0,
                 lif_layer_type = LIFLayer):

        num_layers = num_conv_layers + num_mlp_layers
        
        # If only one value provided, then it is duplicated for each layer
        if len(kernel_size) == 1:   kernel_size = kernel_size * num_conv_layers
        if len(stride) == 1:        stride = stride * num_conv_layers
        if len(pool_size) == 1:     pool_size = pool_size * num_conv_layers
        if len(alpha) == 1:         alpha = alpha * num_layers
        if len(alpharp) == 1:       alpharp = alpharp * num_layers
        if len(beta) == 1:          beta = beta * num_layers
        if len(gamma) == 1:         gamma = gamma * num_layers
        if len(dropout) == 1:       self.dropout = dropout = dropout * num_layers
        if len(conv_channels) == 1: self.conv_channels = conv_channels = conv_channels * num_conv_layers
        if len(mlp_neurons) == 1:   self.mlp_neurons = mlp_neurons = mlp_neurons * num_mlp_layers

        super(LenetDECOLLE, self).__init__()

        # Computing padding to preserve feature size
        padding = (np.array(kernel_size) - 1) // 2  # TODO try to remove padding

        feature_height = input_shape[-2]
        feature_width = input_shape[-1]

        # THe following lists need to be nn.ModuleList in order for pytorch to properly load and save the state_dict
        self.pool_layers = nn.ModuleList()
        self.dropout_layers = nn.ModuleList()

        '''
        Convolution Layer initialization.
        '''
        self.input_shape = input_shape
        conv_channels = [input_shape[0]] + conv_channels
        self.num_conv_layers = num_conv_layers
        self.num_mlp_layers = num_mlp_layers

        for i in range(num_conv_layers):
            feature_height, feature_width = get_output_shape(
                [feature_height, feature_width], 
                kernel_size = kernel_size[i],
                stride = stride[i],
                padding = padding[i],
                dilation = 1)
            feature_height //= pool_size[i]
            feature_width //= pool_size[i]
            base_layer = Re_RAM_Convolution(conv_channels[i], conv_channels[i + 1], kernel_size[i], stride[i], padding[i])
            layer = lif_layer_type(base_layer,
                             alpha=alpha[i],
                             beta=beta[i],
                             gamma=gamma[i],
                             alpharp=alpharp[i],
                             deltat=deltat)
            pool = nn.MaxPool2d(kernel_size=pool_size[i])
            readout = nn.Linear(int(feature_height * feature_width * conv_channels[i + 1]), out_channels)

            # Readout layer has random fixed weights
            for param in readout.parameters():
                param.requires_grad = False
            self.reset_lc_parameters(readout, lc_ampl)

            dropout_layer = nn.Dropout(dropout[i])

            self.LIF_layers.append(layer)
            self.pool_layers.append(pool)
            self.readout_layers.append(readout)
            self.dropout_layers.append(dropout_layer)

        '''
        Multi-Layer Perceptron initialization.
        '''
        mlp_in = int(feature_height * feature_width * conv_channels[-1])
        mlp_neurons = [mlp_in] + mlp_neurons
        for i in range(num_mlp_layers):
            base_layer = Re_RAM_Fully_Connected(mlp_neurons[i], mlp_neurons[i+1])
            layer = lif_layer_type(base_layer,
                             alpha=alpha[i],
                             beta=beta[i],
                             gamma=gamma[i],
                             alpharp=alpharp[i],
                             deltat=deltat,
                             gain= gain)
            readout = nn.Linear(mlp_neurons[i+1], out_channels)

            # Readout layer has random fixed weights
            for param in readout.parameters():
                param.requires_grad = False
            self.reset_lc_parameters(readout, lc_ampl)

            dropout_layer = nn.Dropout(dropout[self.num_conv_layers+i])

            self.LIF_layers.append(layer)
            self.pool_layers.append(nn.Sequential())
            self.readout_layers.append(readout)
            self.dropout_layers.append(dropout_layer)

    def forward(self, input):
        s_out = []
        r_out = []
        u_out = []
        i = 0
        for lif, pool, ro, do in zip(self.LIF_layers, self.pool_layers, self.readout_layers, self.dropout_layers):
            if i == self.num_conv_layers: 
                input = input.view(input.size(0), -1)

            _, u = lif(input)
            u_p = pool(u)
            s_ = smooth_step(u_p)
            sd_ = do(s_)
            r_ = ro(sd_.reshape(sd_.size(0), -1))
            s_out.append(s_) 
            r_out.append(r_)
            u_out.append(u_p)
            input = s_.detach()
            i+=1

        return s_out, r_out, u_out