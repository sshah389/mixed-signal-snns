import torch
from snn_utils import *

loss = torch.nn.SmoothL1Loss()
def decolle_loss(r, tgt):
    loss_tv = 0
    for i in range(len(r)):
        loss_tv += loss(r[i],tgt) 
    return loss_tv
    
def iter_mnist(gen_train, T=1000, max_rate= 20):
    datait = iter(gen_train)
    for raw_input, raw_labels in datait:
        data, labels1h = image2spiketrain(raw_input, raw_labels, max_duration=T, gain=max_rate) # converting data into spikes
        data_t = torch.FloatTensor(data)
        labels_t = torch.Tensor(labels1h)
        yield data_t, labels_t