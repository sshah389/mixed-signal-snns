# Mixed-Signal Spiking Neural Networks (SNNs)
#### Shah Lab, University of Maryland, College Park

### About the Project:
This repository contains the code for the Mixed-Signal SNNs for two Neural Network models: Recurrent and Fully-Connected (FC). The **recurrent** and **fully_connected** folders contain the modules for the Recurrent SNN and the Fully Connected SNN. Both models contains a *non_linear_decolle_model.py* file, which contains the code for the over-arching Lenet-DECOLLE Network model, which can be tuned to include the desired neuron dyanmics and network structures. This Lenet-DECOLLE Network model is based off the DECOLLE model found in the *decolle_model.py* file in both structures. The Lenet-DECOLLE model allows for the use of both Fully-Connected and Convolution Layers. For both models, the *analog_synapses.py* file contains the modules for the hardware-inspired network layers. The *neuron_models.py* file contains the neuron models used in the SNN structure for both network structures, such as the Leaky-Integrate-And-Fire (LIF) Neuron. The *spike_functions.py* file contains the functions used to generate spikes from the membrane potential, such as the Smooth Step Function.

**It is important to note that for each of these files, other custom models or functions can be added to their respective files and called throughout the network model code as necessary.**

Additional necessary files are *snn_utils.py*, which includes spiking dataset loading tools including `get_mnist_loader()` and *utils.py*, which contains supplemental functions used through the models. Also required is *loss.py*, which contains the `decolle_loss()` method used for calculating the total loss based off the individual and the `iter_mnist()` method used to iterate through the MNIST Spiking Dataset in the training and testing loops.

#### How to Train and Test:
This is based off the training and testing done in `mnist_fg_train_fc.ipynb` for the Fully-Connected network found within the **fully_connect** folder. A similar process is used to utilize the Recurrent structure in the *mnist_fg_with_16_16_rnn.ipynb* file within the **recurrent** folder.

##### Loading in the MNIST Dataset:
The MNIST train and test dataset can be loaded using the `get_mnist_loader()` tool, specifying number of dataset partitions (`Nparts`) and desired batch size (in this case 100), using the following:

```python
gen_train = snn_utils.get_mnist_loader(100, Nparts=1, train=True)
gen_test = snn_utils.get_mnist_loader(100, Nparts=1, train=False)
```

##### Creating Dataset Iterator:
For training and testing, a dataset iterator is necessary to iterate through the dataset to pass into the model. The MNIST dataset iterator for the train data can be loaded using the `iter_mnist()` tool as the following:

```python
T = 100 #duration of sequence
data, target = next(loss.iter_mnist(gen_train, T=T))
```
with T representing the spike train duration of the images. In this case, the first batch of data is loaded, which will then be used to initialize the model.

##### Creating the Lenet-DECOLLE Spiking Neural Network model:
The Network model for the Fully-Connected network can then be loaded in using the following:

```python
net = non_linear_decolle_model.LenetDECOLLE(input_shape = data.shape[2:], mlp_neurons = [150,120], num_conv_layers=0, num_mlp_layers=2, alpha=[.95],beta=[.92] ,lc_ampl=.5, out_channels=10).to(device)
```

where `input_shape` represents the shape of input image data, `out_channels` represents the number of output classification categories, and `num_conv_layers` and `num_mlp_layers` represent the intended number of convolution and multi-layer-perceptron (MLP) layers, respectively. `alpha` and `beta` dictate the neuronal dynamics of the LIF Neuron, with other characteristics such as `gamma` customizable as well. `conv_channels` declares the number of channels per convolution layer, while `mlp_neurons` declares the number of neurons per MLP layer. NOTE: The length of `conv_channels` is equal to `num_conv_layers` and the length of `mlp_neurons` is equal to `num_mlp_layers`.

##### Training the Network:
The Spiking Neural Network can then be trained with a `data` and `label` obtained from the iterator created using the `iter_mnist()` for each data batch. Then within the train loop, the dataset must be passed into the model for each timestep `T` in the spike train length as the following (for 30 epochs):

```python
for e in range(30):
    accuracy=[]
    error = []
    for data, label in loss.iter_mnist(gen_train, T=T):
        net.train()
        loss_hist = 0
        data_d = data.to(device)
        label_d = label.to(device)
        net.init(data_d.transpose(0,1), burnin=10)
        readout = 0
        for n in range(T):
            st, rt, ut = net.forward(data_d[n])        
            loss_tv = loss.decolle_loss(rt, st, label_d[n])
            loss_tv.backward()
            opt.step()
            opt.zero_grad()
            loss_hist += loss_tv
            readout += rt[-1]
        error += (readout.argmax(axis=1)!=label_d[-1].argmax(axis=1)).float()
        accuracy+=(readout.argmax(axis=1)==label_d[-1].argmax(axis=1)).float()
```
It is important to note the `net.init(data_d.transpose(0,1), burnin=10)` used to initialize the model's state before each data batch is presented and the use of `decolle_loss()` to appropiately calculate the overall loss for this network structure. `error` and `accuracy` can then be calculated and displayed per epoch using: 

```python
 print('Training Error', torch.mean(torch.Tensor(error)).data)
 print('Training accuracy', torch.mean(torch.Tensor(accuracy)).data)
```
 If necessary, model can be saved after each epoch or after the training with the following:

 ```python
PATH = 'YOUR/DESIRED/PATH'
torch.save(net.state_dict(), PATH)
```

##### Testing the Network and Developing the Confusion Matrix:
For creating the Confusion Matrix graph, some addtional imports are required: `seaborn`, `pandas`, `numpy`, `matplotlib.pyplot` and `confusion_matrix from sklearn.metrics`. Similar to the training loop, the testing loop should calculate the accuracy and accuracy. However, two additional variables, `y_pred` and `y_true` are used to store the predictions and true values later used in the confusion matrix. The lines to note are:

```python
output = (readout.argmax(axis=1)).data.cpu().numpy()
y_pred.extend(output)
labels =(label_d[-1].argmax(axis=1)).data.cpu().numpy()
y_true.extend(labels)
accuracy+=(readout.argmax(axis=1)==label_d[-1].argmax(axis=1)).float()
error += (readout.argmax(axis=1)!=label_d[-1].argmax(axis=1)).float()
```

which is then used to create the confusion matrix in:
```python
classes = ('O', '1', '2', '3', '4',
        '5', '6', '7', '8', '9')
cm = confusion_matrix(y_true, y_pred)
```

Then the confusion matrix can be normalised and then displayed and saved using the seaborn, numpy, and matplotlib libraries as shown:
```python
cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

plt.figure(figsize=(10,10))
sns.heatmap(cm_normalized, annot=True, fmt=".3f", linewidths=.5, square = True, cmap = 'Blues_r');
plt.ylabel('Actual label');
plt.xlabel('Predicted label');

plt.savefig('/YOUR/DESIRED/PATH.svg')  

```

### How to Calculate Power Consumption and Total Area:
This is based off the calculations done in `mnist_fg_test_fc.ipynb` for the Fully-Connected network and the code for the power calculations is contained within the **power** folder. This folder contains the same files as within the other two model folder. However, there are a few key differences utilized in the power and area calculation. The relavant areas for power and area calculation are highlighted by comments for example in both the *analog_synapses.py* and *neuron_models.py* files.

The *non_linear_decolle_model.py* file has been renamed to *non_linear_decolle_model_p.py* to indicate the presence of power and area calculations. The specific code for those two calculations can be seen within the section commented "Total Power and Area Calculation". 

##### Using the Lenet-DECOLLE Spiking Neural Network model with Power and Area Calculations:
The Network model for the Fully-Connected network with the state calculations can then be loaded in using the following:

```python
net = non_linear_decolle_model_p.LenetDECOLLE(input_shape = data.shape[2:], mlp_neurons = [150,120], num_conv_layers=0, num_mlp_layers=2, alpha=[.95],beta=[0] ,lc_ampl=.5, out_channels=10, calc_power = True, 
calc_area = True).to(device)

net.enable_power_calc()
```

It is important to note the `net.enable_power_calc()` after the module creation in addition to `calc_power = True` and `calc_area = True` within the module creation. A model can then be loaded in and tested as previously shown. Finally, after the testing has been conducted, the power and area calculations can be obtained through the following:

```python
power = net.get_avg_power()
print(f' Power: {power*1000} mW')

area= net.get_total_area()
print(f' Area: {area/(1e6)} mm^2')
```

It is important to take note of the units used.