import numpy as np
from torchvision import datasets, transforms
import torch
from torch.utils.data.dataloader import DataLoader
from collections import namedtuple

datasetConfig = namedtuple('config',['image_size','batch_size','data_path'])
input_shape = [28,28,1]

def __gen_ST(N, T, rate, mode = 'regular'):    
    if mode == 'regular':
        spikes = np.zeros([T, N])
        spikes[::(1000//rate)] = 1
        return spikes
    elif mode == 'poisson':
        spikes = np.ones([T, N])        
        spikes[np.random.binomial(1,float(1000. - rate)/1000, size=(T,N)).astype('bool')] = 0
        return spikes
    else:
        raise Exception('mode must be regular or Poisson')
        
def spiketrains(N, T, rates, mode = 'poisson'):
    '''
    *N*: number of neurons
    *T*: number of time steps
    *rates*: vector or firing rates, one per neuron
    *mode*: 'regular' or 'poisson'
    '''
    if not hasattr(rates, '__iter__'):
        return __gen_ST(N, T, rates, mode)
    rates = np.array(rates)
    M = rates.shape[0]
    spikes = np.zeros([T, N])
    for i in range(M):
        if int(rates[i])>0:
            spikes[:,i] = __gen_ST(1, T, int(rates[i]), mode = mode).flatten()
    return spikes

def pixel_permutation(d_size, r_pix=1.0, seed=0):
   n_pix = int(r_pix * d_size)
   np.random.seed(seed*1313)
   pix_sel = np.random.choice(d_size, n_pix, replace=False).astype(np.int32)
   pix_prm = np.copy(pix_sel)
   np.random.shuffle(pix_prm)
   perm_inds = np.arange(d_size)
   perm_inds[pix_sel] = perm_inds[pix_prm]
   return perm_inds

def permute_dataset(dataset, r_pix, seed):
    shape = dataset.data.shape[1:]
    datap = dataset.data.view(-1, np.prod(shape)).detach().numpy()
    perm = pixel_permutation(np.prod(shape), r_pix, seed=seed)
    return torch.FloatTensor(datap[:,perm].reshape(-1,*shape))

def partition_dataset(dataset, Nparts=60, part=0):
    N = len(dataset.data)

    idx = np.arange(N, dtype='int')

    step = (N//Nparts)
    idx = idx[step*part:step*(part+1)]

    td = dataset.data[idx]
    tl = dataset.targets[idx]
    return td, tl

def dynaload(dataset,
          batch_size,
          name,
          DL,
          perm=0.,
          part=0,
          seed=0,
          taskid=0,
          base_perm=.0,
          base_seed=0,
          **loader_kwargs):
        if base_perm>0:
            data = permute_dataset(dataset, base_perm, seed=base_seed)
            dataset.data = data
        if perm>0:
            data = permute_dataset(dataset, perm, seed=seed)
            dataset.data = data

        loader = DL(dataset=dataset,
                    batch_size=batch_size,
                    shuffle=dataset.train,
                    **loader_kwargs)

        loader.taskid = taskid
        loader.name = name +'_{}'.format(part)
        loader.short_name = name
        return loader


def mnist_loader_dynamic(
        config,
        train,
        Nparts=1,
        part=1):
    """Builds and returns Dataloader for MNIST and SVHN dataset."""
    
    transform = transforms.Compose([
                    transforms.Resize(config.image_size),
                    transforms.Grayscale(),
                    transforms.ToTensor(),
                    transforms.Normalize((0.0,), (1.0,))])


    dataset = datasets.MNIST(root=config.data_path, download=True, transform=transform, train = train)
    if Nparts>1:
        data, targets = partition_dataset(dataset, Nparts, part)
        dataset.data = data
        dataset.targets = targets

    dataset_ = dataset
    DL = DataLoader
    name = 'MNIST'

    return dataset_, name, DL

'''
The Main Method used to acquire MNIST Spike Train Dataset.
'''
def get_mnist_loader(
        batch_size,
        train, 
        perm=0.,
        Nparts=1,
        part=0,
        seed=0,
        taskid=0,
        base_perm=.0,
        base_seed=0,
        **loader_kwargs):
    
    config = datasetConfig(image_size = [28,28], batch_size = batch_size, data_path = './data/mnist')
    
    d,name,dl = mnist_loader_dynamic(config, train, Nparts, part)
    
    return dynaload(
          d,
          config.batch_size,
          name,
          dl,
          perm=perm,
          part=part,
          seed=seed,
          taskid=taskid,
          base_perm=base_perm,
          base_seed=base_seed,
          **loader_kwargs)

def image2spiketrain(x,y,gain=50,min_duration=None, max_duration=500):
    y = to_one_hot(y, 10)
    if min_duration is None:
        min_duration = max_duration-1
    batch_size = x.shape[0]
    T = np.random.randint(min_duration,max_duration,batch_size)
    Nin = np.prod(input_shape)
    allinputs = np.zeros([batch_size,max_duration, Nin])
    for i in range(batch_size):
        st = spiketrains(T = T[i], N = Nin, rates=gain*x[i].reshape(-1)).astype(np.float32)
        allinputs[i] =  np.pad(st,((0,max_duration-T[i]),(0,0)),'constant')
    allinputs = np.transpose(allinputs, (1,0,2))
    allinputs = allinputs.reshape(allinputs.shape[0],allinputs.shape[1],1, 28,28)

    alltgt = np.zeros([max_duration, batch_size, 10], dtype=np.float32)
    for i in range(batch_size):
        alltgt[:,i,:] = y[i]

    return allinputs, alltgt

def to_one_hot(t, width):
    t_onehot = torch.zeros(*t.shape+(width,))
    return t_onehot.scatter_(1, t.unsqueeze(-1), 1)